set(ENABLE_fides ON CACHE BOOL "")
set(ENABLE_lookingglass ON CACHE BOOL "")
set(ENABLE_threedxwaresdk ON CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
